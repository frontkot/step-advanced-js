...........

Part of the work from Andrew:

Были использованы дополнительные технилогии, а именно:
- Webpack
- NodeJS Express

1. Webpack

В связи с тем, что мы используем более продвинутый JavaScript, мы пришли к единому мнению, что стоит использовать Gulp или Webpack, сошлись на последнем.
Были использованы следуюшие плагины и лоадеры:

- path
- HtmlWebpackPlugin 
- CleanWebpackPlugin
- MiniCssExtractPlugin
- OptimizeCssAssetsWebpackPlugin
- TerserWebpackPlugin
- CopyWebpackPlugin
- BundleAnalyzerPlugin
- MediaQueryPlugin
- ImageminWebpWebpackPlugin
- CompressionPlugin
- HandlebarsWebpackPlugin
- Loaders: scss, hbs, ...other base loaders.

Для запуска в Dev режиме: "npm run dev", в  Prod: "npm run build".


2. Node JS

Было принято решение усложнить работу и использовать взаимодействие "client -> server -> client" с помощью нашего Web Сервера.
Для того, чтобы сделать связь с сервером, я использовал JasonWebToken, и Express Sessions.
Для хранения данных, была использована оффлайн-база MongoDB.

Были использованы основные модули:

- bcryptjs
- body-parser
- connect-mongodb-session
- cors
- express
- express-session
- jsonwebtoken
- mongoose
- morgan
- nodemon

Схема:

`<div align="center">
  <img src="http://xt3nd3d.tech/scheme.jpg" title="Scheme">
</div>`


Также, добавлены различные проверки в frontend часть, обработка запросов сервером, HandleBars динамический парсинг данных в зависимости от того выполнен ли вход в систему или нет.
Добавлен bootstrap и toastr для flash-уведомлений.
Drag&Drop.


Issues: 

- На более мелких разрешениях экрана есть проблемы с отображением карточек на экране с библиотекой Interact JS.
- Нету валидации инпутов при вводе в заполнении карточек.
- Мелкие неточности, недоработанные до идеала TODO's.
   



Part of the work from Kostya:

JavaScript
1. Написание классов
2. Написание функций вывода и взаимодействия с модальными окнами, фильтрации данных, получение данных из модальных окон и заполнение их из карточек, полученных с сервера, взаимодействие с DOM элементами.
3. Написание взаимодействия обработчика событий с действиями пользователя на странице.

HTML, CSS
1. Некоторые правки для адаптации с js кодом.

Git
1. Создание онлайн репозитория, создание странички сайта на основе ресурсов GitLab.


Part of the work from Vitaliy:

HTML, CSS
1. Написание Html, Css кода.

JavaScript
1. Написание функций взаимодействия с сервером.
2. Правки в коде Javascript, консультирование.